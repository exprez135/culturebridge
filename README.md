# CultureBridge Tulsa
by [Youth Philanthropy Initiative](https://ypitulsa.org) - Cohort XI

### Structure
The repository is split into two major sections:
- The Manual: our philosophy, instructions for the simulation.
- Supplements: materials for running the simulation.

A rough hierarchy is sketched out here:
1. Manual
	1. Cover Page
	2. Contents
	3. [Executive Summary](https://docs.google.com/document/d/1W9YMj3hcsixIns_B0UBqj6HNcuGTFzyP4Rp2Pv7a558)
	3. A Quick Guide to CultureBridge
	5. [PR Packet](https://docs.google.com/document/d/1_J5TsDW4ENbay11EA-b2e8vlvDE1ezH-HUmugqt1Edk/edit?usp=sharing)
	6. Set-up, Supplies, and Kit
	2. Introduction
	3. Simulation
	4. [Debrief](https://docs.google.com/document/d/1pWSpR2DTJfFQkdfF9UPHeH47oju4tfwBxPixZR-xVo4/edit#heading=h.vm9osr1kkq6i)
	5. [Surveys, Data Practices, and Effectiveness](https://docs.google.com/document/d/1Xvq8TbJDbojKMtQyZKlYCyY6O9NKGTSIVt1Jgw1D8cY/edit?usp=sharing)
	6. Our Approach To Public Relations
2. Supplements
	1. [Introduction Presentation](https://docs.google.com/presentation/d/1Mj-Eb9NwJNrXyRdBEr9tPLQNaLVU9-hxTmKqcZEtmKI/edit#slide=id.p)
	2. Character Stories
	3. Activities & Stations
	3. Layout maps
	4. [Surveys](https://docs.google.com/document/d/1OOUBlYzGMfuCo5Ke0nmZr8E-PkU8e0mLoXtfUqDewaI/edit)
	5. [Action Packet](https://docs.google.com/document/d/1XPfGrKbT4B9HiP5F25DOyQ-GIuzeAAdZRYQr3G-lvFA/edit?usp=sharing)
	6. Quick Guides
	    1. Introduction Help Sheet
	    2. Station Instructions
	    3. Debrief Cheat Sheet
	7. Public Relations
	    1. Website
	    4. Promotional Video
	    5. Email Design
	    6. Logos

### Formatting
At the moment, most files are present in the form of exported PDFs. Eventually, original source files (e.g. ODT files) will be released to allow for editing.

### What is the Manual?
The CultureBridge Manual is the master instruction document for the CultureBridge program. It contains the mission, purpose, instructions, ideas, and questions which make up the project. The Manual is split into modules which explain, in detail, that particular section or part of CultureBridge.

### What are the Manual Supplements?
The Manual Supplements are accompanying documents to the CultureBridge Manual which are required for the simulation. These include the introduction presentation, story cards, maps, and surveys.

### What are the Public Relation documents?
The PR documents include files needed for the website, the CultureBridge Promo Video, logos, and other designs used to advertise the project.
