#!/bin/bash

echo -e "\033[0;32mWelcome to the Culture Bridge Deployment Process!\033[0m"
echo -e "\033[0;32mDeploying updates to GitLab...\033[0m"

# Add changes to all directories to git
echo -e "\033[0;32mTracking all git files...\033[0m"
git add .

# Commit changes.
echo -e "\033[0;32mWhat is the custom commit message?\033[0m"
read -r usrmsg

echo -e "\033[0;32mCommitting changes...\033[0m"
msg="Updated $(date)."
if [ $# -eq 1 ]
  then msg="$1"
fi
git commit -m "$usrmsg : $msg"
echo -e "\033[0;32mChanges committed.\033[0m"

# Push source and build repos.
echo -e "\033[0;32mPushing origin master...\033[0m"
git push origin master
echo -e "\033[0;32mPushed.\033[0m"

echo -e "\033[0;32mThank you for joining CultureBridge! Have a wonderful day :)\033[0m"
